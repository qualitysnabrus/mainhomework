import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {

        System.out.println("Ввод текста");
        String a = scan();

        List<String> myList = Arrays.asList(a.toLowerCase().split("\\s+"));
        /*если делать в функциональном стиле
        Map<String, String> words = IntStream.range(0,myList.size()).boxed()
                        .collect(Collectors.toMap (i -> "a"+i, i -> myList.get(i)));*/

        Map<String, String> words = new HashMap<>();
        for (int i = 0; i < myList.size(); i++) {
            words.put("a" + i, myList.get(i));
        }

        removeTheFirstNameDuplicates(words);
    }


    public static void removeTheFirstNameDuplicates(Map<String, String> map) {
        Map<String, String> copy = new HashMap<>(map);

        for (String name : copy.values()) {
            int count = 0;
            for (String nameTmp : map.values()) {
                if (nameTmp.equals(name)) {
                    count++;
                }
            }
            if (count >= 1) {
                map.entrySet().removeIf(entry -> entry.getValue().equals(name));
                System.out.println(name + " встречается " + count + "раз");
            }
        }
    }

    static String scan() {
        String temp = new Scanner(System.in).nextLine();
        temp = temp.trim().replaceAll("\\p{P}", " ");
        return temp;
    }

}
